<?php
define('GUIDERS_SETTINGS_PATH', 'admin/content/guiders/packs');

/**
 * @file
 * CRUD UI interface for the Guiders-JS module
 */

/**
 * Implements hook_page_build().
 */
function guiders_js_page_build(&$page) {
  if (!user_access('access guiders')) {
    return;
  }
  $gp = _guiders_js_get_current_pack();

  if (!empty($gp)) {
    // Adding the required library's files
    $lib_path = libraries_get_path('guiders-js');
    drupal_add_js($lib_path . '/guiders.js');
    drupal_add_css($lib_path . '/guiders.css');

    // Generating the cache id
    $cid = 'guiders-pack-' . $gp->gpid . '-' . $gp->name;

    // Checking if the data exists in the cache
    $cached_js = cache_get($cid, 'cache_guiders');

    // Creating the js because we couldn't find the cached version
    if (empty($cached_js)) {
      // Getting the pack's guiders
      $result = db_select('guiders', 'g')
        ->fields('g')
        ->condition('gpid', $gp->gpid, '=')
        ->execute();

      // Get the raw data into the guiders array
      $guiders = array();
      foreach ($result as $guider) {
        $guiders[] = !empty($guider->data) ? unserialize($guider->data) : new stdClass();
      }

      // Creating the js and attaching the library
      if (count($guiders) > 0) {
        $parsed_js = _guiders_js_parse_guiders($guiders, $gp->auto_start);

        // Setting the data to the cache
        cache_set($cid, $parsed_js, 'cache_guiders', CACHE_TEMPORARY);
      }
    }
    else {
      // Getting the js from the cached data
      $parsed_js = $cached_js->data;
    }

    // Adding the generated script to the page
    drupal_add_js($parsed_js, array('type' => 'inline', 'scope' => 'footer'));
  }
}

/**
 * Returns the active guider pack of the current page, if any.
 */
function _guiders_js_get_current_pack() {
  static $gp;
  if($gp) {
    return $gp;
  }
  // Getting the relevant guider's pack
  // Handling frontpage token replacemnet
  $path = drupal_is_front_page() ? '<front>' : check_url(current_path());

  // Getting the guider's pack that should be displayed in our current path
  $gp = db_select('guiders_packs', 'gp')
    ->fields('gp')
    ->condition('path', $path, '=')
    ->execute()
    ->fetchObject();
  return $gp;
}

/**
 * Implements hook_menu().
 */
function guiders_js_menu() {
  $items = array();
  $base = array(
    'access arguments' => array('administer guiders'),
    'file' => 'guiders_js.admin.inc',
  );

  $items[GUIDERS_SETTINGS_PATH] = array(
    'title' => 'Guiders',
    'description' => 'Add, edit and remove guiders.',
    'page callback' => 'guiders_js_packs_list_page',
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/list'] = array(
    'title' => 'Guider\'s Packs List',
    'page callback' => 'guiders_js_packs_list_page',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -11,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/add'] = array(
    'title' => 'Add Guider Pack',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_packs_add_form'),
    'type' => MENU_LOCAL_TASK,
    'weight' => -10,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs'] = array(
    'title' => 'List Guiders Pack',
    'page callback' => 'guiders_js_list_page',
    'page arguments' => array(5),
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/list'] = array(
    'title' => 'List',
    'page callback' => 'guiders_js_list_page',
    'page arguments' => array(5),
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -11,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_packs_add_form', 5),
    'type' => MENU_CALLBACK,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_delete_confirm', 5),
    'type' => MENU_CALLBACK,
  )+ $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/add'] = array(
    'title' => 'Add Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_add_form', 5),
    'type' => MENU_LOCAL_TASK,
    'weight' => -10,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/%guiders_js/edit'] = array(
    'title' => 'Edit Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_add_form', 5, 6),
    'type' => MENU_CALLBACK,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/manage/%guiders_js_packs/%guiders_js/delete'] = array(
    'title' => 'Delete Guider',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('guiders_js_delete_confirm', 5, 6),
    'type' => MENU_CALLBACK,
  ) + $base;

  $items[GUIDERS_SETTINGS_PATH . '/path/autocomplete'] = array(
    'title' => 'Guider Pack Path Autocomplete',
    'page callback' => 'guiders_js_path_autocomplete',
    'type' => MENU_CALLBACK,
  ) + $base;

  return $items;
}

/**
 * Implements hook_permission().
 */
function guiders_js_permission() {
  return array(
    'administer guiders' => array(
      'title' => t('Administer Guiders'),
      'description' => t('Perform administration tasks for Guiders and Guiders\' Packs.'),
    ),
    'access guiders' => array(
      'title' => t('Access Guiders'),
      'description' => t('Can see guiders in action'),
    ),
  );
}

/*
 * Implements hook_flush_caches().
 */
function guiders_js_flush_caches() {
  return array('cache_guiders');
}

/*
 * Loads a guiders pack by it's unique id
 */
function guiders_js_packs_load($gpid) {
  return db_select('guiders_packs', 'gp')
    ->fields('gp')
    ->condition('gpid', $gpid)
    ->execute()
    ->fetchObject();
}

/*
 * Loads a guiders by it's unique id
 */
function guiders_js_load($gid) {
  return db_select('guiders', 'g')
    ->fields('g')
    ->condition('gid', $gid)
    ->execute()
    ->fetchObject();
}

/**
 * Gets data from the guiders or guiders_packs tables.
 *
 * A genric wrapper for querying data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_get($table, $fields = array()) {
  $res = db_select($table, 'g')->fields('g');

  // Adding the conditions to the query object
  foreach ($fields as $field_name => $field_val) {
    $res = $res->condition($field_name, $field_val);
  }
  return $res->execute();
}

/**
 * Sets data to the guiders or guiders_packs tables.
 *
 * A genric wrapper for setting data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $key
 *   A string containing a priamry key.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_set($table, $key, $fields) {
  return db_merge($table)
    ->key($key)
    ->fields($fields)
    ->execute();
}

/**
 * Deletes data from the guiders or guiders_packs tables.
 *
 * A genric wrapper for deleting data.
 *
 * @param $table
 *   A string containing a table name.
 *
 * @param $fields
 *   An associative array containing fields' names and values to set as conditions
 *   to the query.
 *
 * @return
 *   Query object with the results of the query.
 */
function _guiders_js_del($table, $fields = array()) {
  $res = db_delete('guiders');

  // Adding the conditions to the query object
  foreach ($fields as $field_name => $field_val) {
    $res = $res->condition($field_name, $field_val);
  }
  return $res->execute();
}

/*
 * Dynamically creates Guiders-JS code
 */
function _guiders_js_parse_guiders($guiders, $auto_start = TRUE) {
  $guiders_js = '';
  $size = count($guiders);
  for ($i=0; $i<$size; $i++) {
    $guider = $guiders[$i];

    // Building the buttons json array
    $buttons = '[';
    foreach ($guider['buttons_fs'] as $button) {
      if (isset($button['class']) || isset($button['onclick'])) {
        $button['name'] = check_plain($button['name']);
        $button['other'] = check_plain($button['other']);
        $button['class'] = check_plain($button['class']);
        $button['onclick'] = check_plain($button['onclick']);
      }
      $buttons .= json_encode($button) . ',';
    }
    $buttons .= ']';

    // Sanitize the description field
    $desc = check_markup($guider['desc']['value'], $guider['desc']['format']);

    // Making the desc field be one liner
    $desc = addslashes(preg_replace("/\r?\n/", "<br />", $desc));

    // Appending the full js structure
    $guiders_js .= 'guiders.createGuider({'
      . 'id: "guider_' . $i . '",'
      . 'title: "' . check_plain($guider['title']) . '",'
      . 'description: "' . $desc . '",'
      . 'buttons: ' . ($buttons == "[]" ? '[{"name": "Next"}, {"name": "Close"}]' : $buttons) . ','
      . 'next: "guider_' . ($i + 1) . '", '
      . 'overlay: ' . ($guider['overlay'] == 1 ? 'true' : 'false') . ','
      . 'xButton: ' . ($guider['close'] == 1 ? 'true' : 'false') . ','
      . (!empty($guider['attach']) ? 'attachTo: "' . check_plain($guider['attach']) . '",' : '')
      . (!empty($guider['attach']) && !empty($guider['position']) ? 'position: ' . check_plain($guider['position']) . ',' : '')
      . 'width: "' . check_plain($guider['width']) . '", '
    . '})';

    // Appending "show()" to the first item to start the chain automatically
    if ($i == 0 && $auto_start) {
      $guiders_js .= '.show();';
    }
    else {
      $guiders_js .= ';';
    }
  }

  return $guiders_js;
}

/**
 * Implements hook_block_info().
 */
function guiders_js_block_info() {
  $blocks['guiders_js_link'] = array('info' => 'Guiders help link');
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function guiders_js_block_view($delta) {
  if($delta == 'guiders_js_link') {
    $pack = _guiders_js_get_current_pack();
    if($pack && !$pack->auto_start) {
      drupal_add_js(drupal_get_path('module', 'guiders_js') .'/guiders_js.help.js');
      return array('content' => l(t('Help'), '', array('external' => TRUE, 'fragment' => 'guiders', 'attributes' => array('id' => 'guiders-help'))));
    }
  }
}

function guiders_js_features_api() {
  return array(
    'guiders_js_pack' => array(
      'name' => t('Guiders Pack'),
      'default_hook' => 'guiders_js_pack_features_default',
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'features_source' => TRUE,
      'file' => drupal_get_path('module', 'guiders_js') . '/guiders_js.features.inc',
    ),
  );
}