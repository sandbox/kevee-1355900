<?php

/**
 * @file
 * Features hooks for exporting guider packs.
 */

/**
 * Implements hook_features_export_options().
 */
function guiders_js_pack_features_export_options() {
  $packs = array();
  $guiders_packs = _guiders_js_get('guiders_packs');

  $rows = array();
  foreach ($guiders_packs as $guiders_pack) {
    $packs[$guiders_pack->name] = $guiders_pack->title;
  }
  return $packs;
}

/**
 * Implements hook_features_export.
 */
function guiders_js_pack_features_export($data, &$export, $module_name = '') {
  $pipe = array();
  $map = features_get_default_map('guiders_js_pack');

  foreach ($data as $pack) {
    $export['features']['guiders_js_pack'][$pack] = $pack;
    $export['dependencies']['guiders_js'] = 'guiders_js';
    $export['dependencies']['features'] = 'features';
  }
  
  return $pipe;
}

/**
 * Implements hook_features_export_render().
 */
function guiders_js_pack_features_export_render($module, $data, $export = NULL) {
  $elements = array(
    'name' => TRUE,
    'title' => TRUE,
    'path' => TRUE,
    'auto_start' => TRUE,
  );
  $output = array();
  $output[] = '  $items = array(';
  $guiders_packs = _guiders_js_get('guiders_packs');
  $pack_list = array();
  foreach($guiders_packs as $pack) {
    $pack_list[$pack->name] = (array)$pack;
  }
  foreach ($data as $type) {
    if ($info = $pack_list[$type]) {
      $info = (object)$info;
      $output[] = "    '{$type}' => array(";
      foreach ($elements as $key => $t) {
        $output[] = "      '{$key}' => '{$info->$key}',";
      }
      $guiders = _guiders_js_get('guiders', array('gpid' => $info->gpid));
      $output[] = "      'guiders' => array(";
      foreach($guiders as $guider) {
        $output[] = "        '{$guider->name}' => array(";
        $output[] = "                             'name' => '{$guider->name}',";
        $output[] = "                             'title' => '{$guider->title}',";
        $output[] = "                             'data' => '{$guider->data}',";
        $output[] = "                         ),";
      }
      $output[] = "      ),";
      $output[] = "    ),";
    }
  }
  $output[] = '  );';
  $output[] = '  return $items;';
  $output = implode("\n", $output);
  return array('guiders_js_pack_features_default' => $output);
}

/**
 * Implements hook_features_revert().
 *
 * @param $module
 * name of module to revert content for
 */
function guiders_js_pack_features_revert($module = NULL) {
  
  if ($default_pack = features_get_default('guiders_js_pack', $module)) {
    foreach ($default_pack as $pack_name => $pack_info) {
      _guiders_js_pack_features_clear_pack($pack_name);
    }
    guiders_js_pack_features_enable($module);
    cache_clear_all();
    menu_rebuild();
  }
}

/**
 * Implements hook_features_disable().
 *
 * @param $module
 *   Name of module that has been disabled.
 */
function guiders_js_pack_features_disable($module) {
  
}

function _guiders_js_pack_features_clear_pack($name) {
  $gpid = db_select('guiders_packs', 'p')
             ->fields('p', array('gpid'))
             ->condition('p.name', $name)
             ->execute()
             ->fetchField();
  db_delete('guiders')
    ->condition('gpid', $gpid)
    ->execute();
  db_delete('guiders_packs')
    ->condition('gpid', $gpid)
    ->execute();
}

/**
 * Implements hook_features_enable().
 *
 * @param $module
 *   Name of module that has been enabled.
 */
function guiders_js_pack_features_enable($module) {
  if ($default_types = features_get_default('guiders_js_pack', $module)) {
    foreach ($default_types as $type_name => $pack) {
      $pack = (object)$pack;
      _guiders_js_pack_features_clear_pack($pack->name);
      _guiders_js_set(
        'guiders_packs',
          array('name' => $pack->name),
          array(
            'title' => $pack->title,
            'path'  => $pack->path,
            'auto_start' => ($pack->auto_start) ? 1 : 0,
          )
      );
      $gpid = db_select('guiders_packs', 'p')
             ->fields('p', array('gpid'))
             ->condition('p.name', $pack->name)
             ->execute()
             ->fetchField();
      foreach($pack->guiders as $guider) {
        $guider = (object)$guider;
        _guiders_js_set(
          'guiders',
          array('name' => $guider->name),
          array(
            'gpid'  => $gpid,
            'title' => $guider->title,
            'data'  => $guider->data,
          )
        );
      }
    }
  }
}